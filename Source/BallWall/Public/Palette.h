// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Palette.generated.h"

UCLASS()
class BALLWALL_API APalette : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APalette();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;
public:	

	UPROPERTY(EditAnywhere, Category = Player)
		bool isPlayerController = false;

	UPROPERTY(EditAnywhere, Category = Mesh)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere, Category = Mesh)
		UStaticMeshComponent* BoxMesh;

	UPROPERTY(EditAnywhere, Category = Mesh)
		AActor* Ball;

	UPROPERTY(EditAnywhere, Category = Mesh)
		float Z;

	UPROPERTY(EditAnywhere, Category = Mesh)
		float X;

	float Y;
};
