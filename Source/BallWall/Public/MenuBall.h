// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MenuBall.generated.h"

UCLASS()
class BALLWALL_API AMenuBall: public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AMenuBall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	UPROPERTY(EditAnywhere, Category = Mesh)
		USphereComponent* SphereComponent;

	UPROPERTY(EditAnywhere, Category = Mesh)
		UStaticMeshComponent* SphereMesh;
};
