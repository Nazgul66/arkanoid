// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class BALLWALL_API AMainMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	void BeginPlay()override;

	// Reference UMG Asset in the Editor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wWidget;

	// Variable to hold the widget After Creating it.
	UUserWidget* Widget;	
};
