// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Palette.h"
#include "GameFramework/PlayerController.h"
#include "BallPlayerController.generated.h"

/**
 *
 */
UCLASS()
class BALLWALL_API ABallPlayerController: public APlayerController
{
	GENERATED_BODY()
public:
	ABallPlayerController();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
		APalette* Palette;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		FVector StartPosition;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float Speed = 600;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float maxRightPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement)
		float maxLeftPosition;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> wWidget;

protected:
	UFUNCTION(BlueprintCallable)
		void Moving(float y);

	void BeginPlay()override;

	void SetupInputComponent()override;


	//-600 do 1720
	UPROPERTY(BlueprintReadWrite, Category = Movement)
	float Y;

	UUserWidget* Widget;
private:
};
