// Fill out your copyright notice in the Description page of Project Settings.

#include "BallWall.h"
#include "Public/Palette.h"


// Sets default values
APalette::APalette()
{
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	
	RootComponent = BoxComponent;

	BoxMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
	BoxMesh->AttachTo(BoxComponent);
	BoxMesh->SetAngularDamping(-10);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/MobileStarterContent/Shapes/Shape_Cube.Shape_Cube"));
	if(SphereVisualAsset.Succeeded())
	{
		BoxMesh->SetStaticMesh(SphereVisualAsset.Object);
		BoxMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -50.0f));
		BoxMesh->SetMobility(EComponentMobility::Movable);
		BoxMesh->SetSimulatePhysics(false);
	}

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void APalette::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APalette::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(!isPlayerController)
	{
		//-780 do 1540
		Y = FMath::Clamp<float>(Ball->GetActorLocation().Y, -780, 1540);

		SetActorLocation(FVector(X, Y, Z));
	}
}

