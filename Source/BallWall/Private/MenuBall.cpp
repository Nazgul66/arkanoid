// Fill out your copyright notice in the Description page of Project Settings.

#include "BallWall.h"
#include "Public/MenuBall.h"


// Sets default values
AMenuBall::AMenuBall()
{
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	SphereComponent->InitSphereRadius(51.f);
	RootComponent = SphereComponent;


	static ConstructorHelpers::FObjectFinder<UStaticMesh> SphereVisualAsset(TEXT("/Game/MobileStarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	if(SphereVisualAsset.Succeeded())
	{
		SphereMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SphereMesh"));
		SphereMesh->SetStaticMesh(SphereVisualAsset.Object);
		SphereMesh->AttachTo(SphereComponent);
		SphereMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -50.0f));
		SphereMesh->SetMobility(EComponentMobility::Movable);
		SphereMesh->SetSimulatePhysics(true);
		SphereMesh->SetAngularDamping(-10);
	}

	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AMenuBall::BeginPlay()
{
	Super::BeginPlay();
	SphereComponent->AddImpulse(FVector(0, -101100, 100000));
}

// Called every frame
void AMenuBall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SetActorRotation(FRotator());
}

