// Fill out your copyright notice in the Description page of Project Settings.

#include "BallWall.h"
#include "Public/BallPlayerController.h"
#include "../UMG/Public/Blueprint/UserWidget.h"

ABallPlayerController::ABallPlayerController()
{
	StartPosition = FVector(1000, 700, -350);
}

void ABallPlayerController::BeginPlay()
{
	if(GEngine->IsValidLowLevel())
	{
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, FString("Begin play from code!"));
		for(TObjectIterator<ACameraActor> i; i; ++i)
		{
			if((*i)->GetName() == FString("GameplayCamera"))
			{
				SetViewTarget(*i);
				break;
			}
		}

		if(wWidget) // Check if the Asset is assigned in the blueprint.
		{
			// Create the widget and store it.
			Widget = CreateWidget<UUserWidget>(this, wWidget);

			// now you can use the widget directly since you have a referance for it.
			// Extra check to  make sure the pointer holds the widget.
			if(Widget)
			{
				Widget->AddToViewport();
			}
		}
	}
}

void ABallPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	check(InputComponent);

	//InputComponent->BindAxis(FName("Left"), this, &ABallPlayerController::Moving);
}

void ABallPlayerController::Moving(float y)
{
	Y = y;
	if(GEngine->IsValidLowLevel())
	{
		GEngine->AddOnScreenDebugMessage(-1, .016, FColor::Red, FString::SanitizeFloat(Y));
	}
}
