// Fill out your copyright notice in the Description page of Project Settings.

#include "BallWall.h"
#include "../Public/MainMenuPlayerController.h"
#include "../UMG/Public/Blueprint/UserWidget.h"


void AMainMenuPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if(GEngine->IsValidLowLevel())
	{
		for(TObjectIterator<ACameraActor> i; i; ++i)
		{
			GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, i->GetName());
			if(i->GetName() == "MainMenuCamera")
			{
				SetViewTargetWithBlend((*i));
				GEngine->AddOnScreenDebugMessage(-1, 5, FColor::Red, FString("OK!"));
				break;
			}
		}

		if(wWidget) // Check if the Asset is assigned in the blueprint.
		{
			// Create the widget and store it.
			Widget = CreateWidget<UUserWidget>(this, wWidget);

			// now you can use the widget directly since you have a referance for it.
			// Extra check to  make sure the pointer holds the widget.
			if(Widget)
			{
				Widget->AddToViewport();
			}
		}
	}

	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableMouseOverEvents = true;
}
