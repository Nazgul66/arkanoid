// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "BallWallGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BALLWALL_API ABallWallGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
	ABallWallGameModeBase();
	
	
};
